---
# An instance of the Contact widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: contact

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 130

title: Inscription
subtitle:

# Automatically link email and phone or display as text?
autolink: true

# Email form provider
#   0: Disable email form
#   1: Netlify (requires that the site is hosted by Netlify)
#   2: formspree.io
email_form: 2

netlify:
  # Enable CAPTCHA challenge to reduce spam?
  captcha: false
---

Les consignes sanitaires ne nous permettant pas d'accueillir plus d'une dizaine de personnes, nous sommes contraints de réserver l'accès aux locaux du LISIS uniquement aux intervenant.es. Pour les autres personnes, la participation aux deux journées d'étude devra se faire par visioconférence. L'accès à la "salle de visioconférence" sera limité à une trentaine de participants.

Nous invitons donc les personnes désireuses de participer aux journées d'étude à s'inscrire en utilisant le formulaire de contact ci-dessous ou en nous écrivant directement à l'adresse suivante: [Aymeric.luneau@gmail.com](mailto:Aymeric.luneau@gmail.com).

Enfin, précisez dans votre message si vous pensez assister:

- à la journée du 29/09 ;
- à la journée du 30/09 ;
- Aux 2 journées.

Nous vous remercions de l'intérêt que vous portez à nos journées d'étude.

À bientôt
