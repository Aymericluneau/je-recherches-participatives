---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 15

title: Les recherches participatives face à l'institutionalisation
subtitle: Journées d'étude du 29 et 30 septembre 2020

design:
  columns: "1"
  #background:
    #image: headers/lisis.png
  #  image_darken: 0.6
  #  image_parallax: true
  #  image_position: center
  #  image_size: cover
  #  text_color_light: true
  spacing:
    padding: ["20px", "0", "20px", "0"]

  #header:
    #image: "lisis.png"
    #caption: "Image credit: [**Academic**](https://github.com/gcushen/hugo-academic/)"
---

Soutenues par le [Laboratoire Interdisciplinaire Sciences Innovations Sociétés](http://umr-lisis.fr/), le [Centre Alexandre-Koyré](http://koyre.ehess.fr/) et l'[Institut francilien Recherches Innovations Sociétés](http://ifris.org/), ces deux journées d'étude visent à ouvrir le traitement d'une question de plus en plus présente -- et pressante -- pour les acteurs engagés dans différentes formes de recherche participative au sein de mondes sociaux variés qui est celle de leur institutionalisation au sein des organismes de recherche, de certains segments de politiques publiques et dans des initiatives portées par les acteurs du tiers secteur de la recherche.  Ces deux journées visent à ouvrir un dialogue avec les acteurs du tiers secteur de la recherche afin de comprendre les formes, les conditions de félicité et les effets de l'institutionalisation des recherches et sciences participatives en tenant compte de la pluralité des acteurs, des objets concernés et des formes participatives de production des connaissances.

La première journée adoptera un point de vue « généraliste » en faisant dialoguer chercheurs et acteurs du tiers secteur de la recherche. Tandis que la seconde journée portera sur un segment particulier où recherche, mouvements professionnels et action publique se rencontrent : l'agroécologie. Il s'agira de tirer des enseignements pour poursuivre, ou pas, une exploration sur d'autres thématiques tout en visant une accumulation scientifique fondée sur l'expérience et la réflexivité.

**Comité d'organisation :** Aymeric Luneau, Élise Demeulenaere, Evelyne Lhoste, Lucile Ottolini

[Voir l'argumentaire]({{<ref "post/2020-09-15-argumentaire.md">}})

[Inscription](/je-recherches-participatives/#contact)

{{< figure library="true" src="ifris.jpg" width="40%">}} {{< figure library="true" src="lisis.png" width="40%" >}}
