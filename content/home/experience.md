---
# An instance of the Experience widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: experience

# Activate this widget? true/false
active: false

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Experiences de recherche
subtitle:

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format: Jan 2006

# Experiences.
#   Add/remove as many `experience` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   You can begin a multiline `description` using YAML's `|-`.
experience:
- company: Laboratoire Interdisciplinaire Sciences Innovations Sociétés (INRAE)
  company_url: "http://umr-lisis.fr/"
  date_end: "2019-08-31"
  date_start: "2019-09-16"
  description: |-
    Sous la direction de :
    * Marc Barbier (LISIS -- INRAE), Elise Demeulenaere (CAK -- CNRS).

    Analyse des interactions entre l'institutionnalisation de
    l'agroécologie et celle des recherches participatives dans le cadre de
    l'ANR IDAE (Institutionnalisations des agroécologies)


  location: Noisy-Le-Grand
  title: Ingénieur de recherche

- company: Patrimoine locaux, environnement et globalisation (UMR 208, IRD)
  company_url: "http://www.paloc.fr/fr"
  date_end: "2019-07-31"
  date_start: "2019-06-01"
  description: |-
    Sous la direction de :
    * Régis Ferron, Laure Emperaire, Rigas Arvanitis et Laurent Vidal.

    Analyse des trajectoires professionnelles des chercheurs et
    chercheuses de l'IRD engagés dans des programmes de recherches
    participatifs.

  location: Paris
  title: Chargé de mission

- company: Centre d'écologie et des sciences de la conservation (UMR 7204, CNRS)
  company_url: "https://cesco.mnhn.fr/fr"
  date_end: "2019-02-28"
  date_start: "2017-09-01"
  description: |-
    Sous la direction de :
    * Elise Demeulenaere (CAK -- CNRS),  Stéphanie Duvail (Paloc -- IRD),
    Frédérique Chlous (Paloc -- MNHN) et Romain Julliard (CESCO -- MNHN).

    Analyse des trajectoires des sciences participatives et de leurs postulats épistémologiques.
  location: Paris
  title: Postdoctorant

- company: Université Paris-Ouest Nanterre
  company_url: ""
  date_end: "2017-08-31"
  date_start: "2016-09-01"
  description: Enseignements théoriques et méthodologies aux étudiants de 1ère et 2ème année de sociologie.
  location: Nanterre
  title: Attaché temporaire d'enseignement et de recherche (192 h ETD)

- company: GIS Démocratie et Participation (MSH Paris-Nord)
  company_url: "https://www.participation-et-democratie.fr/"
  date_end: "2016-03-31"
  date_start: "2015-06-01"
  description: |-
    Sous la direction de :
    * Jean-Michel Fourniau (GIS participation), Benoit
    Vergriette et Régine Boutrais (Anses).

    Évaluation des dispositifs participatifs mis en place par l'Agence nationale
    de sécurité sanitaire, de l'environnement et du travail pour
    intégrer les parties prenantes dans la gouvernance de l'agence.
  location: Maison-Alfort
  title: Chargé de mission

- company: Groupe de sociologie pragmatique et réflexive (EHESS)
  company_url: "https://www.gspr-ehess.com/"
  date_end: "2015-03-09"
  date_start: "2010-10-01"
  description: |-
    Sous la direction de :
    * Francis Chateauraynaud, directeur d'étude à l'EHESS

    Thèse de doctorat financé par l'Agende de l'environnement et de la maîtrise
    de l'énergie (ADEME, 2011-2013).

    Analyse des controverses autours des syndromes d'hypersensibilité chimique
    multiple.


  location: Paris
  title: Doctorant
---
