---
title: "Le statut de tiers veilleurs"
event: Session 1.2
event_url:

location:
address:
  street:
  city:
  region:
  postcode:
  country:

summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2020-09-29T13:45:00Z"
date_end: "2020-09-29T14:10:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2020-09-11T00:00:00Z"

authors: [Charlotte_C, aude_L]
tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption:
  focal_point:

#links:
#- icon:
#  icon_pack:
#  name:
#  url:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
#projects:
#- internal-project

# Enable math on this page?
math: true
---
Sciences Citoyennes travaille depuis 2002 à un plaidoyer pour la recherche participative, entendue comme une réelle co-construction des savoirs entre la recherche et le tiers-secteur scientifique. Faisant le constat que la co-construction n’est pas sans défi et ne se décrète pas sans moyen adéquat dans un programme de financement, mais se réalise chemin faisant, par la pratique et la volonté des acteurs et des actrices, l’association a voulu développer le rôle de « tiers-veilleur » pour accompagner les projets dans la co-construction des savoirs. Après une description de la genèse et un bref état des lieux de la définition du « tiers-veilleur », nous nous questionnerons sur les enjeux de la formalisation de « la tiers-veillance ».
