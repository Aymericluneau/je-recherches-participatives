---
title: Produire du savoir en contexte alpin. Recherches collaboratives en microbiologie laitière, 1960-aujourd’hui
event:
event_url:

location:
address:
  street:
  city:
  region:
  postcode:
  country:

summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2020-09-30T10:00:00Z"
date_end: "2020-09-30T10:25:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2020-09-11T00:00:00Z"

authors: [Elise_T]
tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption:
  focal_point:

#links:
#- icon:
#  icon_pack:
#  name:
#  url:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
#projects:
#- internal-project

# Enable math on this page?
math: true
---

Les environnements de montagne sont de longue date la cible de politiques de recherches dites partenariales, qui privilégient des approches en co-construction de problématiques de recherche entre acteurs du développement, professionnels et scientifiques. Les recherches portant sur les filières laitières françaises des Alpes et du Jura sont exemplaires de cette situation. De grands récits retracent le succès de recherches collaboratives depuis leur stade initial informel des années 1960, leur institutionnalisation au milieu des années 1980 jusqu’à aujourd’hui, où coexistent plusieurs dispositifs formels (Mustar 1998; Roybin et al. 2001; Galant et al. 2006; Mollard et al. 2007). En prenant spécifiquement l'exemple de la microbiologie laitière, je présenterai brièvement certains de ces dispositifs de recherches collaboratives. Je défendrai l’idée que le « tournant collaboratif » a eu lieu dans les années 1980 et montrerai qu’il est indissociable des projets de développement territorial portés par l’Etat et les régions à travers un certain nombre d’outils. Je développerai également l’idée que ces recherches participatives restent toutefois minoritaires dans l’ensemble de la microbiologie laitière. Par ailleurs elles ne s’affranchissent pas de conflits autour des différents modes de gestion du vivant qui peuvent être défendus par les différents acteurs. Je m’appuierai pour ce travail sur une analyse de la littérature réflexive publiée par ces différents projets de recherche ainsi que sur un ensemble d’enquêtes qui ont eu lieu entre 2016 et 2018 dans les Alpes du Nord.

{{< cite page="/publication/galant-2006" view="4" >}}
{{< cite page="/publication/mollard-2006" view="4" >}}
{{< cite page="/publication/mustard-1998" view="4" >}}
{{< cite page="/publication/roybin-200116" view="4" >}}
