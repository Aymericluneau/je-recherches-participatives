---
title: Argumentaire
author: [admin, elise_D, evelyne_L, lucile_O, marc_B]
date: '2020-09-15'
slug: argumentaire
categories: []
tags: []
subtitle: ''
summary: ''
authors: []
lastmod: '2020-09-15T16:16:56+02:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Positionnement scientifique
===========================

Parler d'institutionalisation des recherches et sciences participatives
conjugue la rencontre de deux orientations de recherche en sciences
sociales qui ont déjà leur propre histoire intellectuelle : celle qui
porte sur les processus de changement des institutions que l'on appelle
institutionalisation ; et celle qui porte sur ce que nous appelons les
recherches participatives , c'est-à-dire la participation aux processus
de production de connaissances scientifiques de personnes agissant en
tant que citoyens , riverains , autochtones , malades , amateurs et bien
d'autres qualités. Cette participation concerne aussi bien la conception
et la conduite d'une recherche particulière, à la production de données,
la fabrication des objets intermédiaires du travail scientifique, que
l'engagement dans les instances de constructions des politiques
publiques de l'enseignement supérieur, de la recherche et de
l'innovation.

Ces pratiques se sont développées au sein de différentes cultures
épistémiques et mouvements sociaux. Nous pensons à la tradition de
l'amateurat en écologie (Charvolin, 2007), au mouvement de la
recherche-action participative (Hall, 2005), aux mobilisations
collectives de malades dans le champ des sciences biomédicales
(Barbot, 2002; Brown 2007; Akrich et al., 2010), aux
mouvements pour la reconnaissance des droits des populations autochtones
(Roue, 2012), des semences paysannes ([Demeulenaere et al., 2017]({{<ref"/publication/demeulenaere-selection-2017">}}))
ou des FabLab pour ce qui est des
mondes du DIY ([Lhoste et Barbier, 2016]({{<ref"/publication/lhoste-fablabs-2016">}})). L'ouverture des processus de
recherche aux publics est justifiée par la nécessité de produire des
connaissances pertinentes du point de vue des problèmes qu'éprouvent les
acteurs ou au nom de principes de justice, de la même manière que l'ont
été les dispositifs de démocratie participative (GIS Démocratie).



La multiplication des rapports
(GEWISS, 2016; Houllier, 2016; Storup, 2013; Socientize, 2014), l'existence de programmes de financement spécifique (PICRI, REPERE,
millions d'observateurs , Chercheurs citoyens en Nord-Pas de Calais,
ARUC au Canada), la mise en place de cellules ou groupes de travail au
sein des organismes de recherche, l'apparition de patients experts, les
réflexions sur le statut du tiers veilleur ainsi que la structuration d'un "tiers secteur de la recherche" (Alliss, 2017) sont autant d'indices invitant à
penser que les recherches participatifs sont entrées dans un processus
d'institutionalisation au cours de cette décennie. Cette dernière notion évoque l'idée de changements significatifs, de
tournants concernant la carrière d'une cause, d'un problème, d'une
pratique. De marginales ou déviantes , les recherches participatives
deviendraient un mode légitime de production des connaissances
scientifiques voire un nouvel impératif.

Toutefois l'impression d'évidence s'estompe dès lors que l'on cherche à
spécifier les phénomènes que le terme institutionalisation subsume.
Quand et comment commence un tel processus ? A-t-il une fin ? De quels
changements parle-t-on ? Existe-t-il une seule forme
d'institutionalisation ? Est-elle nécessairement liée à une action de
l'État, comme les différents exemples que nous avons donnés le laissent
penser ? Ce sont autant de questions qui méritent d'être examinées. La
complexité est renforcée par le fait que les recherches participatives
sont plurielles, concernent des moments différents de la production des
connaissances et que leur institutionalisation prend effet dans des
sphères différentes.

Il sera nécessaire de reprendre l'abondante littérature pour comprendre
les formes de cette institutionalisation, quels sont ses effets sur les
acteurs, leurs pratiques, la nature des connaissances produites et
saisir en retour les éventuels déplacements que le cas des recherches
participatives opère. En l'occurrence il apparaît que la notion
d'institutionalisation est attachée à trois problématiques
(Ben-David, 1971) : (1) la légitimation d'une pratique,
d'une cause, d'un acteur, etc. ; (2) la structuration d'un champ
autonome à travers l'émergence de segments professionnels
(Bucher et Strauss, 1961) ou d'un système de règles spécifiques
(Galor, 2017) ; (3) l'intégration de ce nouveau
cadre au sein des institutions déjà existantes
(DiMaggio et Powel, 1997).

Analyser l'institutionalisation des
recherches participatives consiste, dans ce cadre, à étudier les
processus à travers lesquels:

-   les recherches participatives deviennent une modalité légitime de
    production des connaissances. En explorant les liens avec le
    mouvement du tiers-secteur de la recherche TSR, la démocratie
    participative ou l'agroécologie, les journées d'étude viseront à
    identifier les dynamiques sociales contribuant à imposer les
    recherches participatives, et les valeurs qu'elles portent, comme
    une nouvelle norme du travail scientifique.

-   Les recherches participatives forment un domaine d'expertise avec
    ses propres règles et arènes. Cette dynamique d'autonomisation est
    illustrée par la proposition de Pierre-Benoit Joly
    (Joly, à paraitre) de créer un secteur des
    recherches participatives au même titre que la recherche normale et
    de la recherche industrielle . En fait, la question est de savoir si
    les processus de segmentation observables au sein de différents
    champs scientifiques convergent pour former un espace de définition
    duquel émerge une identité commune. Il s'agira également d'analyser
    les jeux d'acteurs impliqués dans la normalisation des recherches
    participatives, ses enjeux, ainsi que les formes qu'elle prend. Quel
    est le rôle des acteurs du TSR ? Qui est absent du processus ? Où se
    situent les résistances ? Dans quelle mesure l'édition de règles, au
    travers de guides, de chartes, de lois vient entériner des normes de
    fait .

-   Les recherches participatives transforment et sont transformées par
    leur environnement institutionnel . Ce problème se pose d'autant plus
    que les recherches participatives sont porteuses d'une critique ---
    radicales ou réformatrices --- à l'encontre du fonctionnement de la
    science normale . À moins que l'isomorphisme institutionnel
    (DiMaggio et Powel, 1983) profite d'abord aux institutions existantes,
    parler d'institutionalisation suppose donc que des changements ont
    lieu au sein des institutions chargées de produire des
    connaissances.\
    Si nous pensons à des changements structurels (mode de financements,
    évaluation, acteurs de la programmation de la recherche ), il s'agit
    également d'être attentifs aux effets de cette institutionalisation
    sur le plan épistémique. D'où l'intérêt d'aborder la question des
    interactions entre recherches participatives et agroécologie. Alors
    que la reconnaissance des savoirs paysans en constitue une dimension
    centrale, on cherchera à déterminer si l'institutionalisation de
    l'agroécologie favorise l'expérimentation de démarches
    participatives permettant de faire dialoguer ces savoirs avec les
    connaissances scientifiques, ainsi que la prise en compte de
    nouvelles formes de savoirs dans les travaux d'expertise par
    exemple.




En reprenant ces trois problématiques, les deux journées d'étude ont alors une double ambition. La première
est factuelle . Elle concerne l'analyse des mécanismes
d'institutionalisation des recherches participatives. La seconde est
théorique . Les éléments factuels tirés des analyses permettront de
discuter différents aspects de la notion d'institutionalisation
elle-même. Parce que la question de l'institutionalisation des
recherches participatives est aussi un objet politique travaillé par des
organisations de mouvements sociaux (Alliss, Sciences Citoyennes), il
nous semble nécessaire que ces deux journées d'étude s'appuient autant
sur les expertises des représentants du tiers secteur de la Recherche
(TSR) que sur les expériences de chercheurs issus de cultures
épistémiques différentes.

Enfin, la journée consacrée au cas de l'agroécologie s'explique tout
d'abord par la volonté de bénéficier des recherches menées dans le cadre
du projet ANR institutionalisations des agroécologies qui arrive à son
terme et dans lequel a été spécifiquement traité la question des effets
de cette institutionalisation sur la recomposition des savoirs. Cela
permettra de saisir plus finement les liens entre les dimensions
organisationnelles et épistémiques des dynamiques
d'institutionalisation. Ce focus se veut aussi un essai pour mesurer
l'intérêt de poursuivre, ou pas, la réflexion sur d'autres thématiques.

Insertion dans les travaux de l'IFRIS
-------------------------------------

Cette journée d'étude thématique s'inscrit dans le prolongement des
débats dont l'IFRIS est partie prenante concernant les recherches
participatives. À titre d'exemple, on peut citer le séminaire
international sur la 3e mission de l'enseignement supérieur et de la
recherche (Strasbourg 2017, Dakar 2018), les journées de février 2019
(pour le titre, voir le wiki
<https://fr.wikiversity.org/wiki/Recherche:OpenLabs>).


Dialogue avec le GIS Démocratie et Participation
------------------------------------------------

Au cours de la préparation des journées d'étude, [Jean-Michel Fourniau]({{< ref "/authors/jm_F" >}}), directeur du GIS "Démocratie et Participation", a
proposé de croiser nos interrogations et celles soulevées par le
programme Cit'in sur les expérimentations démocratiques pour la
transition écologique dans lesquelles les recherches participatives sont
apparues comme étant une dimension transversale des différents projets.
Il nous a alors semblé que c'était une opportunité d'initier un dialogue
entre les champs de la recherches participatives et de la démocratie
participative, lesquelles tendent à évoluer parallèlement. Nos
calendriers n'ayant pas permis de construire un événement commun, cette
réflexion commune prendra pour l'instant la forme d'interventions
croisées lors de ces deux journées d'étude et des séminaires du
programme Cit'in.


Bibliographie
=============

{{< cite page="/publication/alliss-prendre-2017" view="4" >}}

{{< cite page="/publication/akrich-sur-2010" view="4" >}}

{{< cite page="/publication/barbot-les-2002" view="4" >}}

{{< cite page="/publication/ben-david-institutionnalization-1971" view="4" >}}

{{< cite page="/publication/brown-toxic-2007" view="4" >}}

{{< cite page="/publication/bucher-professions-1961" view="4" >}}

{{< cite page="/publication/charvolin-sciences-2007" view="4" >}}

{{< cite page="/publication/demeulenaere-selection-2017" view="4" >}}

{{< cite page="/publication/dimaggio-iron-1983" view="4" >}}

{{< cite page="/publication/di-maggio-neo-institutionnalisme-19" view="4" >}}

{{< cite page="/publication/galor-institutionnalization" view="4" >}}

{{< cite page="/publication/gewiss-green-2016" view="4" >}}

{{< cite page="/publication/hall-cold-2005" view="4" >}}

{{< cite page="/publication/houllier-les-2016" view="4" >}}

{{< cite page="/publication/lhoste-fablabs-2016" view="4" >}}

{{< cite page="/publication/mugdal-citizen-2018" view="4" >}}

{{< cite page="/publication/roue-histoire-2012" view="4" >}}

{{< cite page="/publication/socientize-white-2014" view="4" >}}

{{< cite page="/publication/storup-recherche-2013" view="4" >}}

{{< cite page="/publication/weber-concepts-2016" view="4" >}}
