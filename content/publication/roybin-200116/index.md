---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Conduite de recherches pluridisciplinaires en partenariat et apprentissages
  collectifs Le cas du GIS Alpes du Nord"
subtitle: ''
summary: ''
authors:
- Daniel Roybin
- Philippe Fleury
- Claude Béranger
- Didier Curtenaz
tags:
categories: []
date: '2001-01-01'
lastmod: 2020-09-22T12:47:26+02:00
featured: false
draft: false

#volume: 9
#issue: 3

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-22T10:47:26.647617Z'
publication_types:
- 2
abstract:
publication: '*Nature Sciences Sociétés*'
url_pdf: http://www.sciencedirect.com/science/article/pii/S1240130701890452
---
