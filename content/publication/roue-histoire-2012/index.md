---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Histoire et épistémologie des savoirs locaux et autochtones
subtitle: ''
summary: ''
authors:
- Marie Roué
tags:
- '"rapport2"'
- '"savoirs autochtones"'
- '"savoirs écologiques traditionnels"'
- '"savoirs locaux"'
categories: []
date: '2012-11-01'
lastmod: 2020-09-15T17:01:17+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-15T15:01:17.257526Z'
publication_types:
- 2
abstract: Nous tenterons ici une histoire anthropologique de la notion de « savoirs
  locaux », depuis les premiers écrits dans les années 1950 qui se sont intéressés
  aux savoirs botaniques ou zoologiques des peuples « traditionnels » jusqu’à l’explosion
  d’un intérêt pour ces savoirs qui réunit des acteurs aussi disparates que la Banque
  Mondiale, les ONG de conservation et de développement, les gouvernements, les gestionnaires
  de la biodiversité, sans oublier les principaux acteurs, peuples autochtones ou
  groupes spécialisés locaux.En développant une histoire des différents réseaux qui
  ont participé à l’élaboration du concept de savoirs locaux, savoirs écologiques
  traditionnels ou autochtones, nous repérerons les précurseurs en les replaçant dans
  leur contexte heuristique. Nous nous intéresserons aussi aux tendances plus récentes,
  depuis l’inscription des savoirs locaux dans plusieurs conventions internationales,
  en particulier la Convention sur la diversité biologique. Nous examinerons enfin
  la portée épistémologique de l’avènement de savoirs « locaux » qui se confrontent
  ou se conjuguent avec les savoirs scientifiques et les savoirs « profanes ».
publication: '*Revue d’ethnoécologie*'
url_pdf: http://ethnoecologie.revues.org/813
doi: 10.4000/ethnoecologie.813

---
