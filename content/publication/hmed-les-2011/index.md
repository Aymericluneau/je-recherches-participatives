---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Les résistances à l'institutionnalisation
booktitle: "Sociologie de l'institution"
subtitle: ''
summary: ''
authors:
- 'Choukri Hmed'
- Sylvain Laurens
tags: []
categories: [C]
date: '2011-01-01'
lastmod: 2020-09-11T16:09:16+02:00
featured: false
draft: false
weight: 10
publisher: "Belin"
pub_pages: 131--150

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-11T14:09:16.424945Z'
publication_types:
- 6
abstract: ''
---
