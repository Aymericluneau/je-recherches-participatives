---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: La sélection participative à l'épreuve du changement d'échelle. À propos d’une
  collaboration entre paysans sélectionneurs et généticiens de terrain
subtitle: ''
summary: ''
authors: 
- elise_D
- Pierre Rivière
- Alexandre Hyacinthe
- Raphaël Baltassat
- Sofia Baltazar
- Jean-Sébastien Gascuel
- Julien Lacanette
- Hélène Montaz
- Sophie Pin
- Olivier Ranke
- Estelle Serpolay-Besson
- Mathieu Thomas
- Gaëlle Van Frank
- Marc Vanoverschelde
- Camille Vindras-Fouillet
- Isabelle Goldringer
tags:
categories: []
date: '2017-01-01'
lastmod: 2020-09-11T15:54:32+02:00
featured: false
draft: false

pub_pages: 336--346

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-11T13:54:32.339192Z'
publication_types:
- 2
abstract: Dans les années 2000, des agriculteurs désireux de cultiver des blés correspondant
  à leurs besoins et soucieux d’affirmer leur autonomie vis-à-vis de l’industrie semencière,
  ont entrepris de relancer la sélection paysanne à la ferme. Des collaborations ont
  été tissées avec des généticiens de l’Inra, et se sont progressivement développées
  dans le cadre de projets financés. Or les financements de recherche impliquent une
  formalisation des partenariats, des engagements en termes de résultats académiques
  et une augmentation significative de la taille des projets. Dans ce nouveau contexte,
  comment préserver les valeurs d’émancipation paysanne et de justice cognitive sur
  lesquelles la collaboration paysans-chercheurs s’est originellement construite ?
  Cet article aborde la façon dont ce défi se pose concrètement aux acteurs de ces
  projets, et les précautions qu’ils mettent en œuvre pour y faire face.

publication: '*Natures Sciences Sociétés*'
url_pdf: https://www.cairn.info/revue-natures-sciences-societes-2017-4-page-336.htm
doi: 10.1051/nss/2018012
---
