---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Institutionalization
subtitle: ''
summary: ''
authors:
- Katharina Galor
tags: []
categories: []
date: '2017-01-01'
lastmod: 2020-09-15T17:01:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-15T15:01:20.512825Z'
publication_types:
- 6
abstract: Histories of explorations usually focus on the explorers or the director
  of the excavation, as well as the artifacts or sites they uncover. They rarely emphasize
  the institutional setting that quickly emerged as the necessary agent of most archaeological
  endeavors. At stake here are the interaction and interdependency of archaeologists,
  discoveries, and institutions—how these have evolved over time and, most significantly,
  how professionals in their administrative contexts have produced together what I
  argue represents the inseparable interplay of science, knowledge, and ideology.  The
  political climate in the Near East toward the end of the eighteenth century and
  the beginning
publication: '*Finding Jerusalem*'
url_pdf: https://www.jstor.org/stable/10.1525/j.ctt1pq349g.9

pub_pages: 28-42
---
