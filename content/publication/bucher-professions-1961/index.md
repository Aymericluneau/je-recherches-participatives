---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Professions in Process
subtitle: ''
summary: ''
authors:
- Rue Bucher
- Anselm Strauss
tags: []
categories: []
date: '1961-01-01'
lastmod: 2020-09-11T15:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-11T13:54:31.877506Z'
publication_types:
- 2
abstract: A process approach to professions focuses upon diversity and conflict of
  interest within a profession and their implications for change. The model posits
  the existence of a number of groups, called segments, within a profession, which
  tend to take on the character of social movements. Segments develop distinctive
  identities and a sense of the past and goals for the future, and they organize activities
  which will secure an institutional position and implement their distinctive missions.
  In the competition and conflict of segments in movement the organization of the
  profession shifts.
publication: '*American Journal of Sociology*'
url_pdf: http://www.jstor.org/stable/2773729
pub_pages: 325--334
---
