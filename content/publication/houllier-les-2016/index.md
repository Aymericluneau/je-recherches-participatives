---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Les sciences participatives en France. Etat des lieux, bonnes pratiques et
  recommandations
subtitle: ''
summary: ''
authors:
- François Houllier
- Jean-Baptiste Merilhou-Goudard
tags:
- '"rapport2"'
categories: []
date: '2016-01-01'
lastmod: 2020-09-11T15:54:33+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-11T13:54:32.784005Z'
publication_types:
- 4
abstract: Les principaux objectifs de ce rapport sont de produire et d’interpréter
  des données aussi objectives que possible pour caractériser l’ampleur et les spécificités
  du développement des sciences participatives dans le monde. Il s'agit également
  de recueillir et d’analyser des témoignages d’experts et d’acteurs de terrain français
  pour estimer les opportunités et les investissements nécessaires afin de formuler
  des recommandations générales et des propositions concrètes au grain des projets
  ou, plus globalement, à destination des institutions.
publication: ''
publisher: 'Mission sciences participatives'
---
