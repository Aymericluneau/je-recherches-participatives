---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'La relance du fromage de Beaufort'
booktitle: 'Les chercheurs et l’innovation'
subtitle: ''
summary: ''
authors:
- Philippe Mustar
tags: []
categories: []
date: '1998-01-01'
lastmod:
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: ''
publication_types:
- 6
abstract: ''
publisher: 'Éditions Quae'
location: Versailles
url_pdf: https://www.cairn.info/les-chercheurs-et-l-innovation--9782738008206-page-84.htm
pub_pages: 84-115
---
