---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Toxic Exposures. Contested Illnesses and the Environmental Health Movement
subtitle: ''
summary: ''
authors:
- Phil Brown
tags: []
categories: []
date: '2007-01-01'
lastmod: 2020-09-15T17:01:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-15T15:01:18.934045Z'
publication_types:
- 5
abstract: ''
publisher: 'Columbia University Press'
---
