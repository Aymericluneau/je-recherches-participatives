---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: FabLabs
subtitle: 'L’institutionnalisation de Tiers-Lieux du \"soft hacking\"'
summary: ''
authors:
- evelyne_L
- marc_B
tags: []
categories: []
date: '2016-03-01'
lastmod: 2020-09-14T17:27:35+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-14T15:27:34.277319Z'
publication_types:
- 2
abstract: Nous repla&#231;ons le mouvement de cr&#233;ation de nombreux fablabs dans
  une perspective socio-historique en revenant sur la gestation du mod&#232;le invent&#233;
  au MIT et sur sa r&#233;plication en France. Partant, nous restituons les cadres
  et les m&#233;thodes d&#8217;une &#233;tude de terrain conduite au moment de l&#8217;expansion
  des fablabs fran&#231;ais en 2013. En limitant notre enqu&#234;te aux fablabs ouverts
  au public, nous en distinguons deux cat&#233;gories. Nous analysons les activit&#233;s
  d&#8217;interm&#233;diation en leur sein ainsi que le travail d&#8217;institutionnalisation
  r&#233;alis&#233; par leurs fondateurs. &#192; l&#8217;aune d&#8217;une analyse
  compar&#233;e de ces deux types de Tiers-Lieux, nous interpr&#233;tons les fablabs
  comme une forme d&#8217;exp&#233;rimentation collective et distribu&#233;e de l&#8217;innovation
  ouverte qui doit beaucoup aux trajectoires des fondateurs et &#224; leur strat&#233;gie
  de &#171;&#160;soft hacking&#160;&#187; des institutions.
publication: "*Revue d'anthropologie des connaissances*"
url_pdf: https://www.cairn.info/revue-anthropologie-des-connaissances-2016-1-page-43.htm

---
