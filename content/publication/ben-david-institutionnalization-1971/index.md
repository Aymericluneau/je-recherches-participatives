---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: The institutionnalization of science in seventeenth century
subtitle: ''
summary: ''
authors:
- Joseph Ben-David
categories: []
date: '1971-01-01'
lastmod: 2020-09-11T16:09:16+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-07T14:09:15.690900Z'
publication_types:
- 6
abstract: ''
booktitle: 'The scientist’s role in society. A comparative study'
publisher: 'Prentice-Hall'
#location: 'Englewood Cliffs'

pub_pages: "75"
---
