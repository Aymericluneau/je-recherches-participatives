---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Citizen science for environmental policy: development of an EU-wide inventory and analysis of selected practices."
subtitle: 'Final report for the European Commission, DG Environment'
summary: ''
authors:
- Shailendra Mudgal
- Anne Turbé
- Francisco Sanz
- Jorge Barba
- Maite Pelacho
- Fermín Serrano-Sanz
- Lucy Robinson
- Margaret Gold
tags: []
categories: []
date: '2018-11-01'
lastmod: 2020-09-15T17:01:20+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-15T15:01:20.073000Z'
publication_types:
- 4
abstract: ''
publisher: 'European Commission'
---
