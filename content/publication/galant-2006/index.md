---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'Gérard Grosclaude: témoignage Archorales:  les métiers de la recherche, témoignages 12'
subtitle: ''
summary: ''
authors:
- Christian Galant
- Bernard Desbrosses
- Gérard Grosclaude
tags: []
categories: []
date: '2006-01-01'
lastmod:
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-15T15:01:21.047396Z'
publication_types:
- 0
abstract: ''
publication: ''
url_pdf: https://inra-dam-front-resources-cdn.brainsonic.com/ressources/afile/411091-b7819-resource-temoignage-gerard-grosclaude-archorales-12.pdf
doi:

pub_pages:
---
