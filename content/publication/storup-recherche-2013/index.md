---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: La recherche participative comme mode de production des savoirs. Un état des
  lieux des pratiques en France
subtitle: ''
summary: ''
authors:
- Bérangère Storup
- Glen Millot
- Claudia Neubauer
tags:
- '"rapport2"'
categories: []
date: '2013-01-01'
lastmod: 2020-09-11T16:09:15+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-11T14:09:15.022244Z'
publication_types:
- 4
abstract: ''
#publication: ''
publisher: 'Sciences Citoyennes'
---
