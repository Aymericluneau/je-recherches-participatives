---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "*Prendre au sérieux la société de la connaissance. Le livre blanc d'Alliance sciences sociétés*"
subtitle: ''
summary: ''
authors:
- 'Alliss'
tags: []
categories: []
date: '2017-03-01'
lastmod: 2020-09-11T16:09:15+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-11T14:09:15.348876Z'
publication_types:
- 4
abstract: ''
publication: ''
url_pdf: https://uploads.strikinglycdn.com/files/7d940e4b-30b7-4d97-befe-9d5b3fbcb27e/Alliss_Livre%20Blanc_mars%202017.pdf
---
