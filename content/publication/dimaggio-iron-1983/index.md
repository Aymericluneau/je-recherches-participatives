---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'The Iron Cage Revisited: Institutional Isomorphism and Collective Rationality in Organizational Fields'
subtitle: ''
summary: ''
authors:
- Paul J. Di Maggio
- Walter W. Powell
tags: []
categories: []
date: '1983-04-01'
lastmod: 2020-09-15T17:01:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-09-15T15:01:21.047396Z'
publication_types:
- 2
abstract: ''
publication: '*American Sociological Review*'
url_pdf: http://www.jstor.org/stable/2095101?origin=crossref
doi: 10.2307/2095101

pub_pages: 147
---
