---
# Display name
title: Sylvie Blangy

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Ingénieure de recherche

# Organizations/Affiliations to show in About widget
laboratory:
- shortname: CEFE
  name: Centre d'écologie fonctionnelle et évolutive
  url: https://www.cefe.cnrs.fr/fr/recherche/bc/dsse/871-it/693-sylvie-blangy

# Organizations/Affiliations to show in About widget
organizations:
- shortname: CNRS
  role: Ingénieure de recherche
  name: CNRS
  url:
- shortname: GDR Parcs
  role: directrice
  article: du
  name: Groupement de recherche Participatory Action Research and Citizen Sciences
  url: https://websie.cefe.cnrs.fr/gdrparcs/
# Short bio (displayed in user profile at end of posts)
bio: Co-directrice de l'OHMI NUNAVIK (Observatoire Homme Milieu International) au Nord Québec et a été directrice du GDR CNRS PARCS.

# Interests to show in About widget
interests: ""

# Education to show in About widget


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
#social:
#- icon: envelope
#  icon_pack: fas
#  link: mailto:aymeric.luneau@gmail.com
#- icon: https://halshs.archives-ouvertes.fr/public/halshs.png
#  icon_pack: https://halshs.archives-ouvertes.fr/public/halshs.png
#  link: https://twitter.com/GeorgeCushen
#- icon: graduation-cap  # OR `google-scholar`
#  icon_pack: fas  # OR `ai`
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
#email: ""

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
#- Comité d'organisation
- Intervenant.es

# {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}.
---

Sylvie Blangy est Ingénieure de Recherche du CNRS au Centre d’Ecologie Fonctionnelle et Evolutive (CEFE  UMR CNRS ). Elle est co-directrice de l'OHMI NUNAVIK (Observatoire Homme Milieu International) au Nord Québec et a été directrice du GDR CNRS PARCS (Participatory Action Research and Citizen Sciences). Elle travaille depuis 2006 dans les régions arctiques et sub-arctiques en collaboration avec les communautés autochtones Cree, Inuit et Saami sur leurs préoccupations et leurs priorités (impacts sociaux et économiques des mines, sécurité alimentaire, changements climatiques, écotourisme…). Elle développe des projets avec ses partenaires locaux en mettant en relation l’expertise locale et les connaissances scientifiques selon une démarche de Recherche Action Participative. Ces projets sont construits et menés avec et pour les communautés dans le cadre de sa bourse Marie Curie.
