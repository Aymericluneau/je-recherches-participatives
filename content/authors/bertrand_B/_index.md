---
# Display name
title: Bertrand Bocquet

# Username (this should match the folder name)
authors:
- bertrand_B

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Professeur des Universités

# Organizations/Affiliations to show in About widget
laboratory:
- shortname: HT2S
  name: Histoire des Techno-Sciences en Société
  url: http://technique-societe.cnam.fr/bocquet-bertrand-957539.kjsp

organizations:
- shortname: Université de Lille
  role: Professeur des Universités
  name: Université de Lille
  url:

# Short bio (displayed in user profile at end of posts)
bio: Professeur à l'Université de Lille en physique appliquée. Il dirige la Boutique des Sciences Nord de France.

# Interests to show in About widget
interests: ""

# Education to show in About widget


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
#social:
#- icon: envelope
#  icon_pack: fas
#  link: mailto:aymeric.luneau@gmail.com
#- icon: https://halshs.archives-ouvertes.fr/public/halshs.png
#  icon_pack: https://halshs.archives-ouvertes.fr/public/halshs.png
#  link: https://twitter.com/GeorgeCushen
#- icon: graduation-cap  # OR `google-scholar`
#  icon_pack: fas  # OR `ai`
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
#email: ""

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
#- Comité d'organisation
- Intervenant.es

# {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}.
---

Il est Professeur à l'Université de Lille en physique appliquée. Il dirige la Boutique des Sciences Nord de France. Il effectue ses recherches au laboratoire Histoire des technosciences en société (HT2S EA 3716) du CNAM à Paris sur l'analyse et l'expérimentation des interfaces STS (Sciences, Techniques, Société) par l'entrée des recherches participatives, la co-production des connaissances et la citoyenneté scientifique et technique. Il est auteur ou co-auteur de plus de 200 publications dont 118 articles et communications internationales, cinq chapitres de livre et quatre brevets.
