---
# Display name
title: Aymeric Luneau

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Postdoctorant en sociologie

laboratory:
- shortname: LISIS
- name: Laboratoire Interdisciplinaire Sciences Innovations Sociétés
  url: http://umr-lisis.fr/
# Organizations/Affiliations to show in About widget
organizations:
- shortname: INRAE
- name: Institut national de recherche pour l'agriculture, l'alimentation et l'environnement
  url:

# Short bio (displayed in user profile at end of posts)
bio:

# Interests to show in About widget
#interests:


# Education to show in About widget


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: mailto:aymeric.luneau@gmail.com
#- icon: https://halshs.archives-ouvertes.fr/public/halshs.png
#  icon_pack: https://halshs.archives-ouvertes.fr/public/halshs.png
#  link: https://twitter.com/GeorgeCushen
#- icon: graduation-cap  # OR `google-scholar`
#  icon_pack: fas  # OR `ai`
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: gitlab
  icon_pack: fab
  link: https://framagit.org/Aymericluneau
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/aymeric-luneau-b507795b/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
#email: ""

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- Comité d'organisation

# {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}.
---
Aymeric Luneau est postdoctorant en sociologie au LISIS. Il a fait sa thèse sous la direction de Francis Chateauraynaud au Groupe de sociologie pragmatique et réflexive sur les syndromes d’hypersensibilité chimiques et les disputes qu’elles génèrent. Depuis, il s’ intéresse aux transformations des formes d’implication du public dans l’expertise et la recherche scientifique.
