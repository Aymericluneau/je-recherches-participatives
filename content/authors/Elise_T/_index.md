---
# Display name
title: Elise Tancoigne

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Postdoctorante en histoire des sciences et des techniques

# Organizations/Affiliations to show in About widget
Laboratory:
- shortname: Rethinking Science and Public participation
  name: Rethinking Science and Public participation
  url:

organizations:
- shortname: Université de Genève
  role: Postdoctorante
  name: Université de Genève
  url:

# Short bio (displayed in user profile at end of posts)
bio: Ma recherche porte sur les liens entre systèmes alimentaires, innovations techniques et patrimoine.

# Interests to show in About widget
interests: ""

# Education to show in About widget


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
#social:
#- icon: envelope
#  icon_pack: fas
#  link: mailto:aymeric.luneau@gmail.com
#- icon: https://halshs.archives-ouvertes.fr/public/halshs.png
#  icon_pack: https://halshs.archives-ouvertes.fr/public/halshs.png
#  link: https://twitter.com/GeorgeCushen
#- icon: graduation-cap  # OR `google-scholar`
#  icon_pack: fas  # OR `ai`
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
#email: ""

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
#- Comité d'organisation
- Intervenant.es

# {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}.
---
Ma recherche porte sur les liens entre systèmes alimentaires, innovations techniques et patrimoine. Je m'intéresse plus particulièrement à l'histoire de la modernisation de l'industrie laitière, à travers l'étude de la gouvernance des microbes laitiers. Ce travail s'inscrit à l'Université de Genève au sein du projet ERC  "Rethinking Science and Public Participation".

Voir son [blog personnel](https://www.etancoigne.fr/)
