---
# Display name
title: Claire Brossaud

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Chercheure

# Organizations/Affiliations to show in About widget
laboratory:
- shortname: LAURe
  role: Chercheure
  article: au
  name: laboratoire Lyon architecture, urbanisme, recherche
  url: https://www.laure.archi.fr/

# Organizations/Affiliations to show in About widget
organizations:
- shortname: ENSAL
  name: École nationale supérieure d’architecture de Lyon
  url: http://www.lyon.archi.fr/



# Short bio (displayed in user profile at end of posts)
bio: Elle est sociologue, chercheure au laboratoire LAURE à l’École nationale supérieure d’architecture de Lyon.

# Interests to show in About widget
interests: ""

# Education to show in About widget


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
#social:
#- icon: envelope
#  icon_pack: fas
#  link: mailto:aymeric.luneau@gmail.com
#- icon: https://halshs.archives-ouvertes.fr/public/halshs.png
#  icon_pack: https://halshs.archives-ouvertes.fr/public/halshs.png
#  link: https://twitter.com/GeorgeCushen
#- icon: graduation-cap  # OR `google-scholar`
#  icon_pack: fas  # OR `ai`
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
#email: ""

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
#- Comité d'organisation
- Intervenant.es

# {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}.
---

Elle est sociologue, chercheure au laboratoire LAURE (Lyon architecture, urbanisme, recherche) au sein de l’unité mixte de recherche EVS (Environnement, ville, société ; UMR CNRS 5600) à l’École nationale supérieure d’architecture de Lyon. Elle accompagne des innovations sociales et numériques et s’intéresse à la manière dont ces savoirs émergents transforment l’appropriation des espaces bâtis et structurent les imaginaires socio-politiques de la ville. Elle a contribué à la dynamique internationale, nationale et locale des communs avec l’association VECAM et le collectif Lyon en communs. Elle a également cofondé le laboratoire d’expérimentation Coexiscience (sciences en communs).
