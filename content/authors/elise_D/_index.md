---
# Display name
title: Élise Demeulenaere

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Chargée de recherche

laboratory:
- shortname: CAK
  name: Centre Alexandre-Koyré
  url: http://koyre.ehess.fr/

# Organizations/Affiliations to show in About widget
organizations:
- shortname: CNRS
  role: Chargée de recherche
  name: CNRS
  url:

# Short bio (displayed in user profile at end of posts)
bio: Formée en anthropologie sociale, en écologie et aux science studies, Élise Demeulenaere inscrit ses recherches en anthropologie de l’environnement.

# Interests to show in About widget
interests: ""

# Education to show in About widget


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
#social:
#- icon: envelope
#  icon_pack: fas
#  link: mailto:aymeric.luneau@gmail.com
#- icon: https://halshs.archives-ouvertes.fr/public/halshs.png
#  icon_pack: https://halshs.archives-ouvertes.fr/public/halshs.png
#  link: https://twitter.com/GeorgeCushen
#- icon: graduation-cap  # OR `google-scholar`
#  icon_pack: fas  # OR `ai`
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
#email: ""

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- Comité d'organisation

# {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}.
---

Formée en anthropologie sociale, en écologie et aux science studies, Élise Demeulenaere inscrit ses recherches en anthropologie de l’environnement. Elle explore actuellement les mutations contemporaines des rapports à la nature au croisement entre recompositions politiques et identitaires des mondes agricoles et production des connaissances scientifiques, dans le cadre de régimes de savoirs dits ouverts à la société. Elle a co-dirigé l’ouvrage Humanités environnementales (avec G. Blanc et W. Feuerhahn, 2017) ; et publié dans les revues Anthropologie et sociétés (à paraître), Journal of Peasant Studies (2018), Natures Sciences Sociétés (2017).
